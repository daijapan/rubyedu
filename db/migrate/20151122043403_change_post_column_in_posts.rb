class ChangePostColumnInPosts < ActiveRecord::Migration
  def change
  	rename_column :posts, :post, :description
  end
end

class PostsController < ApplicationController

  def index
  	@posts = Post.all
    @post = Post.new
    comment = @post.comments.build
  end

  def show
    @post=Post.find(params[:id])
  end

  def edit
  end

  def new
  	@post = Post.new
  end

  def create
    @post = Post.create(post_params)
    comment = @post.comments.first #
    comment.user = User.last # assign comment to user
    comment.save
    redirect_to :action => "index"
  end

  def post_params
    params.require(:post).permit(:description, comments_attributes:[:body])
  end

  def upvote
    @post = Post.find(params[:id])
    @post.upvote_by current_user
    redirect_to links_path
  end

  def down_votes
    self.votes.where(value: -1).count
  end  

end

class CommentsController < ApplicationController
  def index
    @comment = Comment.all
  end

  def new
    @comment = Comment.new
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def edit
  end
  
  def create
  @comment = Comment.create(comment_params)
    redirect_to :action => "index"
  end

  def comment_params
    params.require(:comment).permit(:body)
  end

  # def create
  # 	@post = Post.find(params[:post_id])
  # 	@comment = @post.comments.create(params[:comment])
  # 	@comment.user_id = current_user.id
  # 	if @comment.save
  # 	redirect_to post_path(@post)
  #   else
  #   	flash.now[:danger] = "error"
  # end

  def comments
  end
  
end

class Post < ActiveRecord::Base
	has_many :comments
	belongs_to :user
	acts_as_votable
	accepts_nested_attributes_for :comments, allow_destroy: true
end
